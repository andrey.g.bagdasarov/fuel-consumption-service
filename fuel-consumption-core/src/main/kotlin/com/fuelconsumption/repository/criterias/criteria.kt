package com.fuelconsumption.repository.criterias

import com.fuelconsumption.domain.FuelType
import java.time.LocalDate

data class DbCriteria(
    val fuelType: FuelType?,
    val dateFrom: LocalDate?,
    val dateTo: LocalDate?,
    val driverId: Int?
)

data class Criteria(
    val fuelType: FuelType?,
    val month: Int?,
    val year: Int?,
    val driverId: Int?
)
