package com.fuelconsumption.repository

import com.fuelconsumption.domain.FuelConsumptionRecord
import com.fuelconsumption.repository.criterias.DbCriteria

interface FuelConsumptionRepository {

    fun save(record: FuelConsumptionRecord): FuelConsumptionRecord

    fun saveAll(records: List<FuelConsumptionRecord>)

    fun findByCriteria(c: DbCriteria): List<FuelConsumptionRecord>
}