package com.fuelconsumption.domain

import java.math.BigDecimal
import java.time.LocalDate

data class FuelConsumptionRecord(
    var fuelType: FuelType,
    var price: BigDecimal,
    var volume: BigDecimal,
    var date: LocalDate,
    var driverId: Int
)

data class FuelConsumptionForMonth(
    var fuelType: FuelType,
    var volume: BigDecimal,
    var date: LocalDate,
    var price: BigDecimal,
    var totalPrice: BigDecimal,
    var driverId: Int
)

data class Statistics(
    var fuelType: FuelType,
    var volume: BigDecimal,
    var averagePrice: BigDecimal,
    var totalPrice: BigDecimal
)

enum class FuelType(val type: String) {
    OCTANE_95("95"),
    OCTANE_98("98"),
    DIESEL("D");

    companion object {
        fun fromString(type: String): FuelType {
            return values().first { it.type == type }
        }
    }
}


