package com.fuelconsumption.usecase

import com.fuelconsumption.domain.FuelConsumptionRecord
import com.fuelconsumption.repository.FuelConsumptionRepository
import com.fuelconsumption.validator.validatePayload

open class SaveFuelConsumptionRecord(private val repository: FuelConsumptionRepository) {

    fun saveFuelRecord(record: FuelConsumptionRecord): FuelConsumptionRecord {
        validatePayload(record)
        return repository.save(record)
    }

    fun saveAllFuelRecords(records: List<FuelConsumptionRecord>) {
        records.forEach { validatePayload(it) }
        repository.saveAll(records)
    }
}