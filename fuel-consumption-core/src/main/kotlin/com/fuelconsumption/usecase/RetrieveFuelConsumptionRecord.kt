package com.fuelconsumption.usecase

import com.fuelconsumption.domain.FuelConsumptionForMonth
import com.fuelconsumption.domain.FuelConsumptionRecord
import com.fuelconsumption.domain.FuelType
import com.fuelconsumption.domain.Statistics
import com.fuelconsumption.repository.FuelConsumptionRepository
import com.fuelconsumption.repository.criterias.DbCriteria
import com.fuelconsumption.repository.criterias.Criteria
import java.math.BigDecimal
import java.time.LocalDate
import java.time.Month
import java.time.Year
import java.util.*

open class RetrieveFuelConsumptionRecord(private val repository: FuelConsumptionRepository) {

    fun getAllFuelRecords(criteria: Criteria): List<FuelConsumptionRecord> =
        repository.findByCriteria(composeBbCriteria(criteria))

    fun getTotalMoneyAmountByMonth(criteria: Criteria): SortedMap<LocalDate, BigDecimal> = repository
        .findByCriteria(composeBbCriteria(criteria))
        .groupBy({ it.date }, { it.price })
        .mapValues { it.value.fold(BigDecimal.ZERO, BigDecimal::add) }
        .toSortedMap()

    fun getConsumptionForMonth(criteria: Criteria): List<FuelConsumptionForMonth> {

        val list = repository.findByCriteria(composeBbCriteria(criteria))
        val resultList = arrayListOf<FuelConsumptionForMonth>()

        list.map {
            resultList.add(
                FuelConsumptionForMonth(
                    it.fuelType,
                    it.volume,
                    it.date,
                    it.price,
                    it.price * it.volume,
                    it.driverId
                )
            )
        }
        return resultList
    }

    fun getStatistics(criteria: Criteria): List<Statistics> {

        val list = repository.findByCriteria(composeBbCriteria(criteria))
        val averages = hashMapOf<FuelType, BigDecimal>()

        list.groupBy { it.fuelType }.forEach { (fuelType, list) ->
            averages[fuelType] = list.map { it.price }.fold(BigDecimal.ZERO, BigDecimal::add) / list.size.toBigDecimal()
        }

        val results = list.fold(ArrayList<Statistics>(),
            { acc, element ->
                val idx = acc.indexOfFirst { it.fuelType == element.fuelType }
                if (idx >= 0) {
                    acc[idx].volume += element.volume
                    acc[idx].totalPrice += element.price
                } else {
                    val stat = Statistics(
                        element.fuelType,
                        element.volume,
                        element.price,
                        element.price
                    )
                    acc.add(stat)
                }
                acc
            }).map {
            it.averagePrice = averages[it.fuelType]!!
            it
        }
        return results
    }

    private fun monthRange(month: Int, year: Int): Pair<LocalDate, LocalDate> {
        val yr = Year.of(year)
        val mth = Month.of(month)
        val lastDate = Month.of(4).length(yr.isLeap)
        return Pair(LocalDate.of(yr.value, mth.value, 1), LocalDate.of(yr.value, mth.value, lastDate))
    }

    private fun composeBbCriteria(webc: Criteria): DbCriteria {
        if (webc.month != null && webc.year != null) {
            val monthRange = monthRange(webc.month, webc.year)
            return DbCriteria(webc.fuelType, monthRange.first, monthRange.second, webc.driverId)
        } else {
            return DbCriteria(webc.fuelType,null, null, webc.driverId)
        }
    }
}