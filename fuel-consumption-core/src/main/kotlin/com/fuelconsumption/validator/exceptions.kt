package com.fuelconsumption.validator

class PriceValidationException(message: String) : Exception(message)
class VolumeValidationException(message: String) : Exception(message)
class DriverIdValidationException(message: String) : Exception(message)