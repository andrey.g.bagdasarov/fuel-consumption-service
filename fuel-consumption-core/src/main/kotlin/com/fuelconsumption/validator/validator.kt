package com.fuelconsumption.validator

import com.fuelconsumption.domain.FuelConsumptionRecord
import java.math.BigDecimal

fun validatePayload(record: FuelConsumptionRecord) {
    if (record.price < BigDecimal.ZERO) {
        throw PriceValidationException("Price below zero")
    }
    if (record.volume < BigDecimal.ZERO) {
        throw VolumeValidationException("Fuel volume below zero")
    }
    if (record.driverId <= 0) {
        throw DriverIdValidationException("Driver ID is incorrect")
    }
}