package com.fuelconsumption.usecase

import com.fuelconsumption.domain.FuelConsumptionForMonth
import com.fuelconsumption.records
import com.fuelconsumption.repository.FuelConsumptionRepository
import com.fuelconsumption.repository.criterias.Criteria
import com.fuelconsumption.repository.criterias.DbCriteria
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import java.time.LocalDate

internal class RetrieveFuelConsumptionRecordTest {

    private lateinit var service: RetrieveFuelConsumptionRecord
    private lateinit var repo: FuelConsumptionRepository

    @BeforeEach
    fun init() {
        repo = mock(FuelConsumptionRepository::class.java)
        service = RetrieveFuelConsumptionRecord(repo)
    }

    @Test
    fun `should find all records when empty criteria`() {

        // given
        val criteria = Criteria(null, null, null, null)
        val dbCriteria = DbCriteria(null, null, null, null)

        // when
        `when`(repo.findByCriteria(dbCriteria)).thenReturn(records)
        val result = service.getAllFuelRecords(criteria)

        //then
        assertThat(result.size).isEqualTo(21)
    }

    @Test
    fun `should return list of fuel consumption records for specified month`() {

        // given
        //both criteria are just stubs
        val criteria = Criteria(null, 4, 2020, null)
        val dbCriteria = DbCriteria(
            null,
            LocalDate.of(2020, 4, 1),
            LocalDate.of(2020, 4, 30),
            null
        )

        val testRecords = records.filter { it.date == LocalDate.of(2019, 1, 2) }
        val testResultSet = arrayListOf<FuelConsumptionForMonth>()

        testRecords.map {
            testResultSet.add(
                FuelConsumptionForMonth(
                    it.fuelType,
                    it.volume,
                    it.date,
                    it.price,
                    it.price * it.volume,
                    it.driverId
                )
            )
        }

        // when
        `when`(repo.findByCriteria(dbCriteria)).thenReturn(testRecords)
        val result = service.getConsumptionForMonth(criteria)

        // then
        assertThat(result).isEqualTo(testResultSet)
    }

    @Test
    fun `should return statistics for each month, list fuel consumption records grouped by fuel type`() {

        // given
        val criteria = Criteria(null, null, null, 1234)
        val dbCriteria = DbCriteria(null, null, null, 1234)

        // when
        `when`(repo.findByCriteria(dbCriteria)).thenReturn(records)
        val result = service.getStatistics(criteria)

        // then
        assertThat(result.size).isEqualTo(3)
    }
}