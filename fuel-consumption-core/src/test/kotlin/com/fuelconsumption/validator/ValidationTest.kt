package com.fuelconsumption.validator

import com.fuelconsumption.domain.FuelConsumptionRecord
import com.fuelconsumption.domain.FuelType
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.time.LocalDate


class ValidationTest {

    @Test
    fun `should validate payload if money amount is less then 0`() {

        // given
        val record = FuelConsumptionRecord(
            FuelType.OCTANE_98,
            BigDecimal(-1),
            BigDecimal(100),
            LocalDate.now(),
            1234
        )

        // then
        assertThatThrownBy { validatePayload(record) }
            .isInstanceOf(PriceValidationException::class.java)
            .hasMessage("Price below zero")
    }

    @Test
    fun `should validate payload volume is less then 0`() {

        // given
        val record = FuelConsumptionRecord(
            FuelType.OCTANE_98,
            BigDecimal(0.99),
            BigDecimal(-1),
            LocalDate.now(),
            1234
        )

        // then
        assertThatThrownBy { validatePayload(record) }
            .isInstanceOf(VolumeValidationException::class.java)
            .hasMessage("Fuel volume below zero")
    }

    @Test
    fun `should validate payload driver ID`() {

        // given
        val record = FuelConsumptionRecord(
            FuelType.OCTANE_98,
            BigDecimal(0.99),
            BigDecimal(100),
            LocalDate.now(),
            -1234
        )

        // then
        assertThatThrownBy { validatePayload(record) }
            .isInstanceOf(DriverIdValidationException::class.java)
            .hasMessage("Driver ID is incorrect")
    }


}