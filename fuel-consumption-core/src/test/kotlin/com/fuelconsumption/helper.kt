package com.fuelconsumption

import com.fuelconsumption.domain.FuelConsumptionForMonth
import com.fuelconsumption.domain.FuelConsumptionRecord
import com.fuelconsumption.domain.FuelType
import java.math.BigDecimal
import java.time.LocalDate
import java.util.*

//21 elements in collection
val records = listOf(
        FuelConsumptionRecord(FuelType.OCTANE_98, Price.OCTANE_98.type, BigDecimal(60.534), LocalDate.of(2019, 1, 2), 1234),
        FuelConsumptionRecord(FuelType.OCTANE_98,
            Price.OCTANE_98.type.add(BigDecimal("0.005")),
            BigDecimal(25.547),
            LocalDate.of(2019, 1, 2),
            1234
        ),
        FuelConsumptionRecord(FuelType.OCTANE_98, Price.OCTANE_98.type, BigDecimal(15.959), LocalDate.of(2019, 2, 19), 1234),
        FuelConsumptionRecord(FuelType.OCTANE_98, Price.OCTANE_98.type, BigDecimal(22.761), LocalDate.of(2019, 2, 20), 1234),
        FuelConsumptionRecord(FuelType.OCTANE_98, Price.OCTANE_98.type, BigDecimal(64.276), LocalDate.of(2019, 3, 5), 1234),
        FuelConsumptionRecord(FuelType.OCTANE_98, Price.OCTANE_98.type, BigDecimal(40.943), LocalDate.of(2019, 3, 7), 1234),
        FuelConsumptionRecord(FuelType.OCTANE_98, Price.OCTANE_98.type, BigDecimal(43.437), LocalDate.of(2019, 4, 13), 1234),

        FuelConsumptionRecord(FuelType.OCTANE_95, Price.OCTANE_95.type, BigDecimal(63.399), LocalDate.of(2019, 1, 1), 1234),
        FuelConsumptionRecord(FuelType.OCTANE_95, Price.OCTANE_95.type, BigDecimal(41.433), LocalDate.of(2019, 2, 1), 1234),
        FuelConsumptionRecord(FuelType.OCTANE_95, Price.OCTANE_95.type, BigDecimal(24.912), LocalDate.of(2019, 5, 7), 1234),
        FuelConsumptionRecord(FuelType.OCTANE_95, Price.OCTANE_95.type, BigDecimal(28.943), LocalDate.of(2019, 6, 27), 1234),
        FuelConsumptionRecord(FuelType.OCTANE_95, Price.OCTANE_95.type, BigDecimal(28.523), LocalDate.of(2019, 6, 22), 1234),
        FuelConsumptionRecord(FuelType.OCTANE_95, Price.OCTANE_95.type, BigDecimal(41.976), LocalDate.of(2019, 7, 14), 1234),
        FuelConsumptionRecord(FuelType.OCTANE_95, Price.OCTANE_95.type, BigDecimal(34.356), LocalDate.of(2019, 7, 29), 1234),

        FuelConsumptionRecord(FuelType.DIESEL, Price.DIESEL.type, BigDecimal(64.962), LocalDate.of(2019, 1, 2), 1234),
        FuelConsumptionRecord(FuelType.DIESEL, Price.DIESEL.type, BigDecimal(67.223), LocalDate.of(2019, 11, 6), 1234),
        FuelConsumptionRecord(FuelType.DIESEL, Price.DIESEL.type, BigDecimal(51.145), LocalDate.of(2019, 8, 6), 1234),
        FuelConsumptionRecord(FuelType.DIESEL, Price.DIESEL.type, BigDecimal(42.576), LocalDate.of(2019, 8, 20), 1234),
        FuelConsumptionRecord(FuelType.DIESEL, Price.DIESEL.type, BigDecimal(11.845), LocalDate.of(2019, 3, 22), 1234),
        FuelConsumptionRecord(FuelType.DIESEL, Price.DIESEL.type, BigDecimal(58.287), LocalDate.of(2019, 10, 28), 1234),
        FuelConsumptionRecord(FuelType.DIESEL, Price.DIESEL.type, BigDecimal(12.110), LocalDate.of(2019, 2, 11), 1234)
    )

enum class Price(val type: BigDecimal) {
    OCTANE_95(BigDecimal("0.960")),
    OCTANE_98(BigDecimal("0.970")),
    DIESEL(BigDecimal("1.006"))
}


fun sortedMoneyAmount(): SortedMap<LocalDate, BigDecimal> =
    records
        .groupBy({ it.date }, { it.price })
        .mapValues { it.value.fold(BigDecimal.ZERO, BigDecimal::add) }
        .toSortedMap()

fun testFuelConsumptionForMonth(): List<FuelConsumptionForMonth> {
    return listOf(
        FuelConsumptionForMonth(
            FuelType.DIESEL,
            BigDecimal(3578.0),
            LocalDate.of(2019, 6, 2),
            Price.DIESEL.type,
            BigDecimal("3603.046"),
            1234
        ),
        FuelConsumptionForMonth(
            FuelType.DIESEL,
            BigDecimal(3578.0),
            LocalDate.of(2019, 6, 3),
            Price.DIESEL.type,
            BigDecimal("3603.046"),
            1234
        ),
        FuelConsumptionForMonth(
            FuelType.DIESEL,
            BigDecimal(3578.0),
            LocalDate.of(2019, 6, 4),
            Price.DIESEL.type,
            BigDecimal("3603.046"),
            1234
        ),
        FuelConsumptionForMonth(
            FuelType.DIESEL,
            BigDecimal(3578.0),
            LocalDate.of(2019, 6, 5),
            Price.DIESEL.type,
            BigDecimal("3603.046"),
            1234
        )
    )
}


