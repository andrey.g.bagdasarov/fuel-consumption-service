plugins {
    kotlin("jvm")
}

dependencies {
    implementation(kotlin("stdlib"))

    testImplementation("org.junit.jupiter:junit-jupiter:5.6.2")
    testImplementation("org.mockito:mockito-core:3.3.3")
    testImplementation("org.assertj:assertj-core:3.15.0")
}

tasks.withType<Test> {
    useJUnitPlatform()
}