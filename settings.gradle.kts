rootProject.buildFileName = "build.gradle.kts"
rootProject.name = "fuel-consumption-service"
include("fuel-consumption-core")
include("persistence-sqlite")
include("spring-app")
