# fuel-consumption-service

A fuel consumption management application for a small transport company. It should be possible to report purchased fuel volumes and retrieve the already inserted information by other contributors. The application initially have only RESTful API with JSON payloads in response, but won't be limited only to this interface in the future. The fuel consumption data will be stored locally in the file or in an embedded database, but it should be possible later to swap current data storage to something else (MySql, Oracle DB etc).
 
* Fuel consumption registration should accept:
* fuel type(Ex. 95, 98 or D)
* price per litter in EUR (Ex. 10.10
* volume in litters (Ex. 12.5)
* date (Ex. 01.21.2018)
* driver ID (Ex. 12345)

It is possible to register one single record or bulk consumption (multiple records in one file, for example multipart/form-data).
