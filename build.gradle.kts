plugins {
    base
    kotlin("jvm") version "1.3.72"
}

allprojects {

    group = "com.fuelconsumptionservice"
    version = "1.0-SNAPSHOT"

    repositories {
        jcenter()
        mavenCentral()
    }
}

dependencies {
    subprojects.forEach {
        archives(it)
    }
    implementation(kotlin("stdlib"))
}
repositories {
    mavenCentral()
}