plugins {
    kotlin("jvm")
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(project(":fuel-consumption-core"))
    implementation("org.jdbi:jdbi3-core:3.13.0")
    implementation("org.jdbi:jdbi3-sqlite:3.1.0")
    implementation("org.jdbi:jdbi3-kotlin:3.13.0")
    implementation("org.jdbi:jdbi3-kotlin-sqlobject:3.13.0")
    implementation("org.xerial:sqlite-jdbc:3.30.1")

    testImplementation("org.junit.jupiter:junit-jupiter:5.6.2")
    testImplementation("org.mockito:mockito-core:3.3.3")
    testImplementation("org.assertj:assertj-core:3.15.0")
}

tasks.withType<Test> {
    useJUnitPlatform()
}