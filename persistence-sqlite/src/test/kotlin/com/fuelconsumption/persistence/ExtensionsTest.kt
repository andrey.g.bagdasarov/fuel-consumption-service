package com.fuelconsumption.persistence

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.format.DateTimeFormatter

internal class ExtensionsTest() {

    @Test
    fun `should format date`() {
        assertThat(LocalDate.of(2020, 4, 25).toPayloadString())
            .isEqualTo("25.04.2020")

    }
}