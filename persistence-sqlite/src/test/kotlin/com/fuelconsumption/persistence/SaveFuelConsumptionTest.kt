package com.fuelconsumption.persistence

import com.fuelconsumption.domain.FuelConsumptionRecord
import com.fuelconsumption.domain.FuelType
import com.fuelconsumption.repository.criterias.DbCriteria
import org.assertj.core.api.Assertions.assertThat
import org.jdbi.v3.core.Jdbi
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.sql.SQLException
import java.time.LocalDate

internal class SaveFuelConsumptionTest {

    private var dao: FuelConsumptionDao
    private var jdbi: Jdbi

    init {
        jdbi = InitJDBI("application.test.properties").getJDBI();
        dao = FuelConsumptionDao("application.test.properties")
    }

    @Test
    fun `should save record in DB`() {
        // given
        val record = FuelConsumptionRecord(
            FuelType.DIESEL,
            BigDecimal("0.907"),
            BigDecimal("12.0"),
            LocalDate.of(2020, 7, 25),
            123
        )

        // when
        val res = dao.save(record)

        // then
        assertThat(res.driverId).isEqualTo(record.driverId)
    }

    @Test
    fun `should save multiple records in DB`() {
        // given
        val records = listOf<FuelConsumptionRecord>(
            FuelConsumptionRecord(
                FuelType.OCTANE_98,
                BigDecimal("1.007"),
                BigDecimal("22.0"),
                LocalDate.of(2020, 6, 1),
                356
            ),
            FuelConsumptionRecord(
                FuelType.OCTANE_98,
                BigDecimal("1.007"),
                BigDecimal("52.0"),
                LocalDate.of(2020, 6, 3),
                356
            )
        )

        // when
        dao.saveAll(records)

        // then
        val res = dao.findByCriteria(
            DbCriteria(
                null,
                null,
                null,
                356
            )
        )

        assertThat(res.size).isEqualTo(2)
    }

    @AfterEach
    fun clean() {
        jdbi.useHandle<SQLException> { handle ->
            run {
                handle.execute("delete from consumption_records;")
            }
        }
    }
}
