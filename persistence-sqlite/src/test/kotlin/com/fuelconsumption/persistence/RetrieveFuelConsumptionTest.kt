package com.fuelconsumption.persistence

import com.fuelconsumption.domain.FuelType
import com.fuelconsumption.repository.criterias.DbCriteria
import org.assertj.core.api.Assertions
import org.jdbi.v3.core.Jdbi
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.sql.SQLException
import java.time.LocalDate

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RetrieveFuelConsumptionTest {

    private var dao: FuelConsumptionDao
    private var jdbi: Jdbi

    init {
        jdbi = InitJDBI("application.test.properties").getJDBI();
        dao = FuelConsumptionDao("application.test.properties")
        beforeAll()
    }

    private fun beforeAll() {
        dao.saveAll(records)
    }

    @Test
    fun `should query by given date range`() {
        // given
        val criteria = DbCriteria(
            null,
            LocalDate.of(2020, 5, 1),
            LocalDate.of(2020, 8, 30),
            null
        )

        // when
        val res = dao.findByCriteria(criteria)

        // then
        Assertions.assertThat(res.size).isEqualTo(5)
    }

    @Test
    fun `should query by given date range and fuel type`() {
        // given
        val criteria = DbCriteria(
            FuelType.DIESEL,
            LocalDate.of(2020, 6, 1),
            LocalDate.of(2020, 8, 30),
            null
        )

        // when
        val res = dao.findByCriteria(criteria)

        // then
        Assertions.assertThat(res.size).isEqualTo(3)
    }

    @Test
    fun `should query by given driver Id`() {
        // given
        val criteria = DbCriteria(
            null,
            null,
            null,
            1234
        )

        // when
        val res = dao.findByCriteria(criteria)

        // then
        Assertions.assertThat(res.size).isEqualTo(2)
    }


    @Test
    fun `should query all records when criteria is nulls`() {
        // given
        val criteria = DbCriteria(
            null,
            null,
            null,
            null
        )

        // when
        val res = dao.findByCriteria(criteria)

        // then
        Assertions.assertThat(res.size).isEqualTo(records.size)
    }

    @AfterAll
    private fun clean() {
        jdbi.useHandle<SQLException> { handle ->
            run {
                handle.execute("delete from consumption_records;")
            }
        }
    }
}