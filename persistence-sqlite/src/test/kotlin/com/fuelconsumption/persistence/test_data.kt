package com.fuelconsumption.persistence

import com.fuelconsumption.domain.FuelConsumptionRecord
import com.fuelconsumption.domain.FuelType
import java.math.BigDecimal
import java.time.LocalDate

val records = listOf(
    FuelConsumptionRecord(
        FuelType.OCTANE_95,
        BigDecimal(0.890),
        BigDecimal(63.399),
        LocalDate.of(2020, 1, 1),
        1234
    ),
    FuelConsumptionRecord(
        FuelType.DIESEL,
        BigDecimal(1.006),
        BigDecimal(28.523),
        LocalDate.of(2020, 6, 22),
        1234
    ),
    FuelConsumptionRecord(
        FuelType.OCTANE_95,
        BigDecimal(0.890),
        BigDecimal(41.433),
        LocalDate.of(2020, 2, 1),
        7654
    ),
    FuelConsumptionRecord(
        FuelType.OCTANE_95,
        BigDecimal(0.890),
        BigDecimal(24.912),
        LocalDate.of(2020, 5, 7),
        5678
    ),

    FuelConsumptionRecord(
        FuelType.OCTANE_98,
        BigDecimal(0.907),
        BigDecimal(28.943),
        LocalDate.of(2020, 6, 27),
        5555
    ),

    FuelConsumptionRecord(
        FuelType.DIESEL,
        BigDecimal(1.006),
        BigDecimal(41.976),
        LocalDate.of(2020, 7, 14),
        8907
    ),
    FuelConsumptionRecord(
        FuelType.DIESEL,
        BigDecimal(1.006),
        BigDecimal(34.356),
        LocalDate.of(2020, 7, 29),
        5584
    )
)