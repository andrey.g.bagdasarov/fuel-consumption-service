package com.fuelconsumption.persistence

import com.fuelconsumption.domain.FuelConsumptionRecord
import com.fuelconsumption.persistence.mapper.FuelConsumptionRecordMapper
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys
import org.jdbi.v3.sqlobject.statement.SqlQuery
import org.jdbi.v3.sqlobject.statement.SqlUpdate
import org.jdbi.v3.sqlobject.statement.UseRowMapper

interface FuelConsumptionRecordRepository {

    @SqlUpdate
        (
        "INSERT INTO CONSUMPTION_RECORDS (fuel_type,price,volume,date,driver_id) " +
                "VALUES (:record.fuelType.type ,:record.price,:record.volume, strftime('%Y-%m-%d', :record.date/1000, 'unixepoch', 'localtime') ,:record.driverId)"
    )
    @GetGeneratedKeys
    fun save(record: FuelConsumptionRecord): Int

    @SqlQuery("SELECT fuel_type,price,volume,date,driver_id FROM consumption_records WHERE id = :id")
    @UseRowMapper(FuelConsumptionRecordMapper::class)
    fun getFuelConsumptionRecordById(id: Int): FuelConsumptionRecord


    @SqlQuery(
        "SELECT * from consumption_records " +
                "WHERE (:fuelType is null or fuel_type = :fuelType) AND date BETWEEN coalesce(:dateFrom, date) AND coalesce(:dateTo, date) AND (:driverId is null or driver_id = :driverId)"
    )
    @UseRowMapper(FuelConsumptionRecordMapper::class)
    fun getByCriteria(fuelType: String?, dateFrom: String?,  dateTo: String?, driverId: Int?): List<FuelConsumptionRecord>

}