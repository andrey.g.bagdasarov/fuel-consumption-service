package com.fuelconsumption.persistence.mapper

import com.fuelconsumption.domain.FuelConsumptionRecord
import com.fuelconsumption.domain.FuelType
import com.fuelconsumption.persistence.toPayloadString
import org.jdbi.v3.core.mapper.RowMapper
import org.jdbi.v3.core.statement.StatementContext
import java.sql.ResultSet
import java.time.LocalDate

class FuelConsumptionRecordMapper : RowMapper<FuelConsumptionRecord> {


    override fun map(rs: ResultSet, ctx: StatementContext?): FuelConsumptionRecord {
        return FuelConsumptionRecord(
            FuelType.fromString(rs.getString("fuel_type")),
            rs.getBigDecimal("price"),
            rs.getBigDecimal("volume"),
            LocalDate.parse(rs.getString("date")),
            rs.getInt("driver_id")
        )
    }
}