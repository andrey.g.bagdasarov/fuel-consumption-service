package com.fuelconsumption.persistence

import com.fuelconsumption.domain.FuelConsumptionRecord
import com.fuelconsumption.repository.FuelConsumptionRepository
import com.fuelconsumption.repository.criterias.DbCriteria
import java.sql.SQLException

class FuelConsumptionDao(var propertiesPath: String = "") : FuelConsumptionRepository {

    private val CREATE_CONSUMPTION_RECORD_TBL = "create table if not exists consumption_records\n" +
            "(\n" +
            "    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
            "    fuel_type text not null,\n" +
            "    price real,\n" +
            "    volume real,\n" +
            "    date text not null,\n" +
            "    driver_id int not null\n" +
            ")"

    private val CREATE_INDEX_CONSUMPTION_RECORD =
        "create unique index if not exists consumption_records_id_uindex on consumption_records (id)"

    val jdbi = if (propertiesPath.isEmpty()) InitJDBI().getJDBI() else InitJDBI(propertiesPath).getJDBI()
    private val dao = jdbi.onDemand(FuelConsumptionRecordRepository::class.java)

    init {
        jdbi.useHandle<SQLException> { handle ->
            run {
                handle.execute(CREATE_CONSUMPTION_RECORD_TBL)
                handle.execute(CREATE_INDEX_CONSUMPTION_RECORD)
            }
        }
    }

    override fun save(record: FuelConsumptionRecord): FuelConsumptionRecord =
        dao.getFuelConsumptionRecordById(dao.save(record))


    override fun saveAll(records: List<FuelConsumptionRecord>) {
        records.forEach { r ->
            dao.save(r)
        }
    }

    override fun findByCriteria(c: DbCriteria): List<FuelConsumptionRecord> {
        return dao.getByCriteria(
            c.fuelType?.type,
            c.dateFrom?.toString(),
            c.dateTo?.toString(),
            c.driverId
        )
    }
}
