package com.fuelconsumption.persistence

import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.sqlobject.kotlin.KotlinSqlObjectPlugin
import org.sqlite.SQLiteDataSource
import java.io.IOException
import java.util.*
import kotlin.system.exitProcess


open class InitJDBI(var PROPERTIES_PATH: String = "application.properties") {
    private val properties = Properties()

    fun getJDBI(): Jdbi {
        try {
            val inputStream = this.javaClass.classLoader.getResourceAsStream(PROPERTIES_PATH)
            inputStream.use { input ->
                if (input == null) {
                    println("Sorry, unable to find $PROPERTIES_PATH")
                    exitProcess(-1)
                }
                properties.load(input)
            }
        } catch (ex: IOException) {
            ex.printStackTrace()
        }

        val dataSource = SQLiteDataSource()
        dataSource.url = properties.getProperty("db.url")

        val jdbi = Jdbi.create(dataSource)
        jdbi.installPlugin(KotlinSqlObjectPlugin());
        return jdbi
    }
}