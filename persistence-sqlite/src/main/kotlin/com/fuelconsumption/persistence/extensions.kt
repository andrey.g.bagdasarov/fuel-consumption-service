package com.fuelconsumption.persistence

import java.time.LocalDate
import java.time.format.DateTimeFormatter

fun LocalDate.toPayloadString(): String {
    val formatter = DateTimeFormatter.ofPattern("dd.MM.uuuu")
    return this.format(formatter)
}