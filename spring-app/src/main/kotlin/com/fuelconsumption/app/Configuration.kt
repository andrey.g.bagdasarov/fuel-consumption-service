package com.fuelconsumption.app.config

import com.fuelconsumption.persistence.FuelConsumptionDao
import com.fuelconsumption.persistence.InitJDBI
import com.fuelconsumption.usecase.RetrieveFuelConsumptionRecord
import com.fuelconsumption.usecase.SaveFuelConsumptionRecord
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket


@Configuration
open class Configuration(var env: Environment) {

    @Bean
    open fun dao(): FuelConsumptionDao{
        if (env.activeProfiles.contains("test")){
           return FuelConsumptionDao("application.test.properties")
        }
        return FuelConsumptionDao()
    }

    @Bean
    open fun saveUseCase(dao: FuelConsumptionDao) = SaveFuelConsumptionRecord(dao)

    @Bean
    open fun retrieveUseCase(dao: FuelConsumptionDao) = RetrieveFuelConsumptionRecord(dao)

    @Bean
    open fun api(): Docket? {
        return Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.any())
            .build()
    }
}