package com.fuelconsumption.app.dto

import java.math.BigDecimal

data class FuelConsumptionRecordDto(
    var fuelType: String = "",
    var price: BigDecimal = BigDecimal.ZERO,
    var volume: BigDecimal = BigDecimal.ZERO,
    var date: String = "",
    var driverId: Int = 0
)


data class FuelConsumptionForMonthDto(
    var fuelType: String = "",
    var volume: BigDecimal = BigDecimal.ZERO,
    var date: String = "",
    var price: BigDecimal = BigDecimal.ZERO,
    var totalPrice: BigDecimal = BigDecimal.ZERO,
    var driverId: Int = 0
)

data class StatisticsDto(
    var fuelType: String = "",
    var volume: BigDecimal = BigDecimal.ZERO,
    var averagePrice: BigDecimal = BigDecimal.ZERO,
    var totalPrice: BigDecimal = BigDecimal.ZERO
)

data class Error(
    val code: Int,
    val message: String = "",
    val description: String = "",
    val infoUrl: String = ""
)