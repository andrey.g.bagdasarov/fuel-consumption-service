package com.fuelconsumption.app.service

import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fuelconsumption.app.dto.FuelConsumptionForMonthDto
import com.fuelconsumption.app.dto.FuelConsumptionRecordDto
import com.fuelconsumption.domain.FuelConsumptionForMonth
import com.fuelconsumption.domain.FuelConsumptionRecord
import com.fuelconsumption.domain.FuelType
import com.fuelconsumption.persistence.toPayloadString
import org.springframework.web.multipart.MultipartFile
import java.io.IOException
import java.time.LocalDate
import java.time.format.DateTimeFormatter


@Throws(IOException::class)
private fun <T> jsonMapsToPojo(json: String, dtoClass: Class<T>?): T {
    val mapper = ObjectMapper()
    mapper.registerModule(JavaTimeModule())
    mapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
    return mapper.readValue(json, dtoClass)
}


fun fileToRecordsList(file: MultipartFile): List<FuelConsumptionRecord> {
    val array = arrayOf<FuelConsumptionRecordDto>()
    val string = String(file.bytes)
    val dtos = jsonMapsToPojo(string, array::class.java)
    return dtos.map { recordDtoToRecord(it) }
}


fun parseToLocalDate(value: String): LocalDate {
    val formatter = DateTimeFormatter.ofPattern("dd.MM.uuuu")
    val p = formatter.parse(value)
    return LocalDate.from(p)
}

fun recordDtoToRecord(p: FuelConsumptionRecordDto): FuelConsumptionRecord {
    return FuelConsumptionRecord(
        FuelType.fromString(p.fuelType),
        p.price,
        p.volume,
        parseToLocalDate(p.date),
        p.driverId
    )
}

fun recordToRecordDto(r: FuelConsumptionRecord): FuelConsumptionRecordDto {
    return FuelConsumptionRecordDto(
        r.fuelType.type,
        r.price,
        r.volume,
        r.date.toPayloadString(),
        r.driverId
    )
}


fun consumptionForMonthToDto(fcm: FuelConsumptionForMonth):FuelConsumptionForMonthDto{
 return FuelConsumptionForMonthDto(
     fcm.fuelType.type,
     fcm.volume,
     fcm.date.toPayloadString(),
     fcm.price,
     fcm.totalPrice,
     fcm.driverId
 )
}
