package com.fuelconsumption.app.controller

import com.fuelconsumption.app.dto.Error
import com.fuelconsumption.validator.DriverIdValidationException
import com.fuelconsumption.validator.PriceValidationException
import com.fuelconsumption.validator.VolumeValidationException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ExceptionHandlingController {

    @ExceptionHandler(Exception::class)
    fun defaultHandler(ex: Exception): ResponseEntity<Any> {
        return internalServerError(ex.message ?: "")
    }

    @ExceptionHandler(PriceValidationException::class)
    fun priceValidationExceptionHandler(ex: PriceValidationException): ResponseEntity<Any> {
        return badRequest(ex.message ?: "")
    }

    @ExceptionHandler(VolumeValidationException::class)
    fun fuelVolumeValidationExceptionHandler(ex: VolumeValidationException): ResponseEntity<Any> {
        return badRequest(ex.message ?: "")
    }

    @ExceptionHandler(DriverIdValidationException::class)
    fun driverIdValidationExceptionHandler(ex: DriverIdValidationException): ResponseEntity<Any> {
        return badRequest(ex.message ?: "")
    }

    private fun internalServerError(message: String): ResponseEntity<Any> =
        ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(Error(HttpStatus.INTERNAL_SERVER_ERROR.value(), message))

    private fun badRequest(message: String): ResponseEntity<Any> =
        ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(Error(HttpStatus.BAD_REQUEST.value(), message))
}