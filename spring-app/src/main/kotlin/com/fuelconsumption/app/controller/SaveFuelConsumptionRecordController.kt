package com.fuelconsumption.app.controller

import com.fuelconsumption.app.dto.FuelConsumptionRecordDto
import com.fuelconsumption.app.service.fileToRecordsList
import com.fuelconsumption.app.service.recordDtoToRecord
import com.fuelconsumption.app.service.recordToRecordDto
import com.fuelconsumption.usecase.SaveFuelConsumptionRecord
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile

@RestController()
class SaveFuelConsumptionRecordController(private val saveRecord: SaveFuelConsumptionRecord) {

    @PostMapping("/record")
    fun saveFuelConsumptionRecord(@RequestBody payload: FuelConsumptionRecordDto): FuelConsumptionRecordDto {
        val saved = saveRecord.saveFuelRecord(recordDtoToRecord(payload))
        return recordToRecordDto(saved)
    }

    @PostMapping("/records")
    fun saveFuelConsumptionRecords(@RequestBody records: List<FuelConsumptionRecordDto>) {
        saveRecord.saveAllFuelRecords(records.map { recordDtoToRecord(it) })
    }

    @PostMapping("/file")
    fun saveRecordFile(@RequestParam("file") file: MultipartFile?): ResponseEntity<Any> {
        if (file == null) {
            return ResponseEntity.noContent().build()
        }
        val records = fileToRecordsList(file)
        saveRecord.saveAllFuelRecords(records)
        return ResponseEntity.accepted().build()
    }

}