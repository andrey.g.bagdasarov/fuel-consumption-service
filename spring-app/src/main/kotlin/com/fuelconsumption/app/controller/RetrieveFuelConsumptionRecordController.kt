package com.fuelconsumption.app.controller

import com.fuelconsumption.app.dto.FuelConsumptionForMonthDto
import com.fuelconsumption.app.dto.StatisticsDto
import com.fuelconsumption.app.service.consumptionForMonthToDto
import com.fuelconsumption.domain.FuelConsumptionForMonth
import com.fuelconsumption.repository.criterias.Criteria
import com.fuelconsumption.usecase.RetrieveFuelConsumptionRecord
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal
import java.time.LocalDate
import java.util.*

@RestController()
class RetrieveFuelConsumptionRecordController(private val retrieveRecord: RetrieveFuelConsumptionRecord) {

    @GetMapping("/money")
    fun getMoneyGroupedByMonth(@RequestParam("driverId") driverId: String?): SortedMap<LocalDate, BigDecimal> {
        return retrieveRecord.getTotalMoneyAmountByMonth(Criteria(null, null, null, driverId?.toInt()))
    }

    @GetMapping("/records")
    fun getConsumptionRecordsForMonth(
        @RequestParam("month", required = false) month: String?,
        @RequestParam("year", required = false) year: String?,
        @RequestParam("driverId") driverId: String?
    ): List<FuelConsumptionForMonthDto> {
        val result = retrieveRecord.getConsumptionForMonth(Criteria(null, month?.toInt(), year?.toInt(), driverId?.toInt()))
        return result.map { consumptionForMonthToDto(it) }
    }


    @GetMapping("/statistics")
    fun getStatistics(
        @RequestParam("driverid", required = false) driverId: String?
    ): List<StatisticsDto> {
        val criteria = Criteria(
            null,
            null,
            null,
            driverId?.toInt()
        )
        val res = retrieveRecord.getStatistics(criteria)
        return res.map {
            StatisticsDto(
                it.fuelType.type,
                it.volume,
                it.averagePrice,
                it.totalPrice
            )
        }
    }
}
