package com.fuelconsumption.app.controller

import com.fuelconsumption.app.dto.Error
import com.fuelconsumption.app.dto.FuelConsumptionRecordDto
import com.fuelconsumption.domain.FuelConsumptionRecord
import com.fuelconsumption.domain.FuelType
import com.fuelconsumption.persistence.FuelConsumptionDao
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.core.io.FileSystemResource
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import java.io.File
import java.math.BigDecimal
import java.math.MathContext
import java.sql.SQLException
import java.time.LocalDate


@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class SaveFuelConsumptionRecordControllerTest {

    @Autowired
    lateinit var template: TestRestTemplate

    @Autowired
    lateinit var dao: FuelConsumptionDao

    @AfterAll
    fun clean(){
        dao.jdbi.useHandle<SQLException> { handle ->
            run {
                handle.execute("delete from consumption_records;")
            }
        }
    }

    @Test
    fun `should store record in DB`() {
        // given
        val record = FuelConsumptionRecordDto(
            FuelType.OCTANE_98.type,
            BigDecimal(0.972).round(MathContext.DECIMAL64).setScale(3),
            BigDecimal(10.501).round(MathContext.DECIMAL64).setScale(3),
            "01.04.2020",
            7771
        )
        val resp = template.postForEntity("/record", record, FuelConsumptionRecordDto::class.java)

        assertThat(resp.body).isEqualTo(record)
    }

    @Test
    fun `should upload data from file`() {
        // given
        val str: String = this::class.java.getClassLoader().getResource("test.json").file
        val file = File(str)
        val resource = FileSystemResource(file)
        val body: MultiValueMap<String, Any> = LinkedMultiValueMap()
        body.add("file", resource)

        val headers = HttpHeaders()
        headers["Content-Type"] = "multipart/form-data"

        val requestEntity = HttpEntity(body, headers)

        // when
        val resp = template.postForEntity("/file", requestEntity, String::class.java);

        // then
        assertThat(resp.statusCode).isEqualTo(HttpStatus.ACCEPTED)
    }

    @Test
    fun `should return error message when price is bellow zero`() {
        // given
        val record = FuelConsumptionRecordDto(
            FuelType.OCTANE_98.type,
            BigDecimal(-1),
            BigDecimal(10.0),
            "20.04.2020",
            7771
        )

        // when
        val resp = template.postForEntity("/record", record, Error::class.java)

        // then
        assertThat(resp.body.code).isEqualTo(HttpStatus.BAD_REQUEST.value())
    }

    @Test
    fun `should return error message when fuel volume is bellow zero`() {
        // given
        val record = FuelConsumptionRecordDto(
            FuelType.OCTANE_98.type,
            BigDecimal("0.972"),
            BigDecimal("-1"),
            "20.04.2020",
            7771
        )

        // when
        val resp = template.postForEntity("/record", record, Error::class.java)

        // then
        assertThat(resp.body.code).isEqualTo(HttpStatus.BAD_REQUEST.value())
    }
}