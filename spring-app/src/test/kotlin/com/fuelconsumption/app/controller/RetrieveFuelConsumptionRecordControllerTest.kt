package com.fuelconsumption.app.controller

import com.fuelconsumption.app.records
import com.fuelconsumption.domain.FuelConsumptionRecord
import com.fuelconsumption.domain.FuelType
import com.fuelconsumption.persistence.FuelConsumptionDao
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.context.annotation.Profile
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.web.util.UriComponentsBuilder
import java.math.BigDecimal
import java.sql.SQLException
import java.time.LocalDate

@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class RetrieveFuelConsumptionRecordControllerTest {

    @Autowired
    lateinit var template: TestRestTemplate

    @Autowired
    lateinit var dao: FuelConsumptionDao

    @BeforeAll
    fun beforeAll(){
        //clean db
        dao.jdbi.useHandle<SQLException> { handle ->
            run {
                handle.execute("delete from consumption_records;")
            }
        }
        // save new set of test data
        dao.saveAll(records)
    }

    @Test
    fun `should return statistics data by driverId`() {

        val builder = UriComponentsBuilder
            .fromUriString("/statistics")
            .queryParam("driverid", 1234)

        val url = builder.build().toUri()

        val resp = template.exchange(
            builder.build().toUri(),
            HttpMethod.GET,
            HttpEntity.EMPTY,
            List::class.java
        );

        assertThat(resp.body.size).isEqualTo(2)
    }


    @Test
    fun `should return statistics data`() {

        val builder = UriComponentsBuilder
            .fromUriString("/statistics")

        val resp = template.exchange(
            builder.build().toUri(),
            HttpMethod.GET,
            HttpEntity.EMPTY,
            List::class.java
        );

        assertThat(resp.body.size).isEqualTo(3)
    }

    @Test
    fun `list of fuel consumption records for specified month`() {

        val builder = UriComponentsBuilder
            .fromUriString("/records")
            .queryParam("month", 7)
            .queryParam("year", 2020)


        // when
        val resp = template.exchange(
            builder.build().toUri(),
            HttpMethod.GET,
            HttpEntity.EMPTY,
            List::class.java
        );

        // then
        assertThat(resp.body.size).isEqualTo(2)
    }

}