package com.fuelconsumption.app.service

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.LocalDate


internal class ConvertersKtTest {

    @Test
    fun `should parse string to LocalDate`() {
        val res = parseToLocalDate("01.04.2020")

        assertThat(res).isEqualTo(LocalDate.of(2020, 4, 1))
    }
}