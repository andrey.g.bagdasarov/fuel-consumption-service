plugins {
    kotlin("jvm")
    id("org.springframework.boot") version "2.2.6.RELEASE"
    id("io.spring.dependency-management") version "1.0.7.RELEASE"
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation ("io.springfox:springfox-swagger-ui:2.9.2")
    implementation ("io.springfox:springfox-swagger2:2.9.2")

    /*For some reason spring app module can't get jdbi from persistence module
      while gradle build
      this import here is "brute-force" workaround
    */
    implementation("org.jdbi:jdbi3-core:3.13.0")

    implementation(project(":fuel-consumption-core"))
    implementation(project(":persistence-sqlite"))

    testImplementation("org.springframework.boot:spring-boot-starter-test:2.2.6.RELEASE")
}

tasks.withType<Test> {
    useJUnitPlatform()
}


tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}